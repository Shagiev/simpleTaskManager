<?php
declare(strict_types=1);

namespace Shagiev\SimpleTaskManagerBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Shagiev\SimpleTaskManagerBundle\Entity\Repository\TaskRepository;
use Shagiev\SimpleTaskManagerBundle\Entity\Task;

/**
 * Class TaskHandler
 * @package Shagiev\SimpleTaskManagerBundle\Service
 */
class TaskHandler
{
    /** @var  ObjectManager */
    protected $entityManager;
    /** @var  TaskRepository */
    protected $repository;

    /**
     * @param \Doctrine\Common\Persistence\ObjectManager $entityManager
     */
    public function setEntityManager(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository('ShagievSimpleTaskManagerBundle:Task');
    }

    /**
     * @param $taskId
     * @param bool $isDone
     */
    public function setTaskDone($taskId, bool $isDone)
    {
        $task = $this->repository->getTask($taskId);
        $this->setDoneRecursively($task, $isDone);
        if ($task->getParent()) {
            $this->checkParentDoneRecursively($task->getParent(), $isDone);
        }
    }

    /**
     * If all children tasks are done, set parent task done too
     * If parent task was done, but child task set to undone, set parent task undone too
     *
     * @param Task $parent
     * @param bool $isDone
     */
    public function checkParentDoneRecursively(Task $parent, bool $isDone)
    {
        if (is_null($parent)) {
            return;
        }

        $goUp = false;
        if (!$isDone && $parent->getIsDone()) {
            $goUp = true;
        } elseif ($isDone && !$parent->getIsDone()) {
            $subtaskList = $this->repository->getSubtaskList($parent->getId());
            $allDone = count($subtaskList) && !(bool)count(array_filter(
                    $subtaskList,
                    function (Task $task) {
                        return !$task->getIsDone();
                    }));
            if ($allDone) {
                $goUp = true;
            }
        }
        if ($goUp) {
            $parent->setIsDone($isDone);
            $this->entityManager->persist($parent);
            if ($parent->getParent()) {
                $this->checkParentDoneRecursively($parent->getParent(), $isDone);
            }
        }
    }

    /**
     * Set child tasks to such
     * @param Task $task
     * @param bool $isDone
     */
    protected function setDoneRecursively(Task $task, bool $isDone)
    {
        $task->setIsDone($isDone);
        $this->entityManager->persist($task);

        $subtaskList = $this->repository->getSubtaskList($task->getId());
        foreach ($subtaskList as $subtask) {
            $this->setDoneRecursively($subtask, $isDone);
        }
    }

    /**
     * @param $taskId
     */
    public function deleteTask($taskId)
    {
        $task = $this->repository->getTask($taskId);
        $this->deleteTaskRecursively($task);
        $this->entityManager->flush();

        //If other subtasks of parent task are done, then set parent task done
        if ($task->getParent()) {
            $this->checkParentDoneRecursively($task->getParent(), true);
        }
    }

    /**
     * @param Task $task
     */
    protected function deleteTaskRecursively(Task $task)
    {
        if (!$task) {
            return;
        }
        $subtaskList = $this->repository->getSubtaskList($task->getId());
        foreach ($subtaskList as $subtask) {
            $this->deleteTaskRecursively($subtask);
        }
        $this->entityManager->remove($task);
    }

    /**
     * @param Task[] $taskList
     * @return Task[]
     */
    public function organizeTaskTree(array $taskList)
    {
        /** @var Task[] $_taskList */
        $_taskList = [];
        /** @var Task[] $result */
        $result = [];

        foreach ($taskList as $task) {
            $_taskList[$task->getId()] = $task;
        }
        foreach ($_taskList as $task) {
            $parent = $task->getParent();
            if ($parent && isset( $_taskList[ $parent->getId() ] )) {
                $_taskList [ $task->getParent()->getId() ] ->addSubtask($task);
            } else {
                $result[] = $task;
            }
        }
        return $result;
    }
}
