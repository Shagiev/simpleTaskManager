<?php
declare(strict_types=1);

namespace Shagiev\SimpleTaskManagerBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Shagiev\SimpleTaskManagerBundle\Entity\Task;
use Shagiev\SimpleTaskManagerBundle\Form\TaskType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class DefaultController
 * @package Shagiev\SimpleTaskManagerBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()
            ->getRepository('ShagievSimpleTaskManagerBundle:Task');

        $session = new Session();
        $show = $session->get('show-tasks') ?: 'all';

        /** @var Task[] $taskList */
        $taskList = $repository->getTaskList($show);

        $formList = $this->getFormList($taskList);

        $taskHandler = $this->get('stm.task_handler');
        $taskList = $taskHandler->organizeTaskTree($taskList);

        return $this->render('ShagievSimpleTaskManagerBundle:Default:index.html.twig', [
            'tasks' => $taskList,
            'show' => $show,
            'forms' => $formList
        ]);
    }

    /**
     * @param Task[] $taskList
     * @return FormView[]
     */
    protected function getFormList(array $taskList)
    {
        /** @var FormView[] $formList */
        $formList = [];
        $formList['new']['new'] = $this->createForm(TaskType::class, new Task())
            ->createView();

        foreach ($taskList as $task) {
            $formList[$task->getId()] = $this->createForm(TaskType::class, $task)
                ->createView();
            $formList['new'][$task->getId()] = $this->createForm(TaskType::class, new Task())
                ->createView();
        }
        return $formList;
    }

    /**
     * @param Request $request
     * @param int $parentId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request, $parentId)
    {
        $task = new Task();

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityMgr = $this->getDoctrine()->getManager();

            $task = $form->getData();

            if (!$task->getId() && $parentId) {
                //new task with parent
                $parentTask = is_null($parentId) ? null : $this->getTask($parentId);
                $task->setParent($parentTask);

                //if parent task was done, make it undone
                $taskHandler = $this->get('stm.task_handler');
                $taskHandler->setEntityManager($entityMgr);
                $taskHandler->checkParentDoneRecursively($parentTask, false);
            }

            $entityMgr->persist($task);
            $entityMgr->flush();
            $this->get('session')->getFlashBag()
                ->add('task-notice', 'The task was successfully saved. Rock on!');
        }
        return $this->redirect($this->generateUrl('shagiev_simple_task_manager_homepage'));
    }

    /**
     * @param $taskId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($taskId)
    {
        /** @var ObjectManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $taskHandler = $this->get('stm.task_handler');
        $taskHandler->setEntityManager($entityManager);
        $taskHandler->deleteTask($taskId);

        $entityManager->flush();

        $this->get('session')->getFlashBag()
            ->add('task-notice', 'The task was successfully deleted');

        return $this->redirect($this->generateUrl('shagiev_simple_task_manager_homepage'));
    }

    /**
     * @param string $option
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($option)
    {
        if (!in_array($option, ['all', 'active', 'completed'])) {
            $this->createNotFoundException("Not found status '$option'");
        }
        $session = new Session();
        $session->set('show-tasks', $option);

        return $this->redirect($this->generateUrl('shagiev_simple_task_manager_homepage'));
    }

    /**
     * @param int|string $taskId
     * @return Task
     */
    protected function getTask($taskId)
    {
        $repository = $this->getDoctrine()->getManager()
            ->getRepository('ShagievSimpleTaskManagerBundle:Task');
        $task = $repository->find($taskId);
        if (!$task) {
            throw $this->createNotFoundException('Unable to find task');
        }
        return $task;
    }

    /**
     * @param $taskId
     * @param bool $isDone
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setDoneAction($taskId, bool $isDone)
    {
        /** @var ObjectManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $taskHandler = $this->get('stm.task_handler');
        $taskHandler->setEntityManager($entityManager);
        $taskHandler->setTaskDone($taskId, $isDone);

        $entityManager->flush();

        return $this->redirect($this->generateUrl('shagiev_simple_task_manager_homepage'));
    }
}
