//Toggle plus-minus
$('[data-toggle="collapse"]').on('click', function() {
    var $this = $(this),
        $parent = typeof $this.data('parent')!== 'undefined' ? $($this.data('parent')) : undefined;
    if($parent === undefined) { /* Just toggle my  */
        $this.find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
        return true;
    }

    /* Open element will be close if parent !== undefined */
    var currentIcon = $this.find('.glyphicon');
    currentIcon.toggleClass('glyphicon-plus glyphicon-minus');
    $parent.find('.glyphicon').not(currentIcon).removeClass('glyphicon-minus').addClass('glyphicon-plus');
});

$('.edit-button').on('click', function () {
    var $this = $(this);
    var id = $this.attr('data-id');
    var task_display = $('div[data-id="'+id+'"] .task-display').first();
    var task_form = $('div[data-id="'+id+'"] .task-form').first();
    task_display.hide();
    task_form.show();
});

$('.cancel-edit-button').on('click', function () {
    var $this = $(this);
    var id = $this.attr('data-id');
    var task_form = $('div[data-id="'+id+'"] .task-form').first();
    var task_display = $('div[data-id="'+id+'"] .task-display').first();
    task_form.hide();
    task_display.show();
});
