<?php
declare(strict_types=1);

namespace Shagiev\SimpleTaskManagerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Shagiev\SimpleTaskManagerBundle\Entity\Task;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class TaskRepository
 * @package Shagiev\SimpleTaskManagerBundle\Entity\Repository
 */
class TaskRepository extends EntityRepository
{
    /**
     * @param string $showOption Can be all/active/completed
     * @return mixed
     */
    public function getTaskList($showOption = 'all')
    {
        $qBuilder = $this->createQueryBuilder('t')
            ->addOrderBy('t.createdAt', 'DESC');

        if ($showOption === 'active') {
            $qBuilder->where('t.isDone = FALSE');
        } elseif ($showOption === 'completed') {
            $qBuilder->where('t.isDone = TRUE');
        }

        return $qBuilder->getQuery()->getResult();
    }

    /**
     * @param $taskId
     * @return Task
     */
    public function getTask($taskId)
    {
        /** @var Task $task */
        $task = $this->find($taskId);
        if (!$task) {
            throw new NotFoundHttpException('Unable to find task');
        }
        return $task;
    }

    /**
     * @param $taskId
     * @return array
     */
    public function getSubtaskList($taskId)
    {
        $qBuilder = $this->createQueryBuilder('t')
            ->where('t.parent = :taskId')
            ->setParameter('taskId', $taskId);

        return $qBuilder->getQuery()->getResult();
    }
}
