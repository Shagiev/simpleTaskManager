<?php
declare(strict_types=1);

namespace Shagiev\SimpleTaskManagerBundle\Form;

use Symfony\Component\Form\{
    AbstractType,
    Extension\Core\Type\SubmitType,
    Extension\Core\Type\TextType,
    FormBuilderInterface
};
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TaskType
 * @package Shagiev\SimpleTaskManagerBundle\Form
 */
class TaskType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
//            ->add('description', TextareaType::class)
//            ->add('parent', HiddenType::class)
            ->add('save', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Shagiev\SimpleTaskManagerBundle\Entity\Task'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shagiev_simple_task_manager_bundle_task_type';
    }
}
